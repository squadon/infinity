import {PNG_NAME} from '../constants';
import headerLine from './collection/header-line.png';
import logo from './collection/logo.png';
import showcaseBackground from './collection/showcase_backgorund.png';
import youtubeCapture from './collection/youtube_capture.png';

export const PNGs = {
  [PNG_NAME.HEADER_LINE]: headerLine,
  [PNG_NAME.LOGO]: logo,
  [PNG_NAME.SHOWCASE_BACKGROUND]: showcaseBackground,
  [PNG_NAME.YOUTUBE_CAPTURE]: youtubeCapture,
};

export default PNGs;