/*
 #variable#
 */

const fr = {
  navbar: {
    theme: 'Theme',
  },
  home: {
    introduction: {
      one: 'compose ton site web',
      two: 'en quelques clics'
    },
    firstStep: 'premier pas',
    see: 'voir',
    possibilities: 'possibilités',
    tryIt: 'essayer',
  },
  tuto: {
    first: 'selectionnez votre thème favoris',
    second: 'ajoutez à votre thème vos articles préférés',
    third: 'discutons ensemble des détails',
    fourth: 'recevez sous quelques jours votre site web',
    pickNow: 'choisir maintenant',
    howItWorks: 'comment ça marche ?',
  },
  imgAlt: {
    icon: 'icon',
    arrow: 'arrow',
    arrowLeft: 'arrow-left',
    arrowRight: 'arrow-right',
    preview: 'preview',
    video: 'video',
  },
};

export default fr;