export const en = {
  navbar: {
    theme: 'bla',
  },
  home: {
    introduction: {
      one: 'compose your website',
      two: 'in few clicks'
    },
    firstStep: 'first step',
    see: 'see',
    possibilities: 'possibilities',
    tryIt: 'try it',
  },
  tuto: {
    first: 'select your favorite theme',
    second: 'add your preferred items to your theme',
    third: 'discuss together about details',
    fourth: 'get your website in a few days',
    pickNow: 'pick now',
    howItWorks: 'how it works ?',
  },
  imgAlt: {
    icon: 'icon',
    arrow: 'arrow',
    arrowLeft: 'arrow-left',
    arrowRight: 'arrow-right',
    preview: 'preview',
    video: 'video',
  }
};

export default en;