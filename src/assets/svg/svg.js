import {SVG_NAME} from '../constants';
import reactLogo from './collection/reactLogo.svg';
import ukFlag from './collection/uk.svg';
import frFlag from './collection/france.svg';
import trash from './collection/trash.svg';
import validate from './collection/validate.svg';
import edit from './collection/edit.svg';
import eye from './collection/eye.svg';
import paint from './collection/paint.svg';
import click from './collection/click.svg';
import arrow from './collection/arrow.svg';
import arrowGreen from './collection/arrow_green.svg';
import addShopping from './collection/add_shopping.svg';
import cardPayment from './collection/card-payment.svg';
import langageIcon from './collection/language.svg';

export const SVGs = {
  [SVG_NAME.REACT_LOGO]: reactLogo,
  [SVG_NAME.FR_FLAG]: frFlag,
  [SVG_NAME.UK_FLAG]: ukFlag,
  [SVG_NAME.TRASH]: trash,
  [SVG_NAME.VALIDATE]: validate,
  [SVG_NAME.EDIT]: edit,
  [SVG_NAME.EYE]: eye,
  [SVG_NAME.PAINT]: paint,
  [SVG_NAME.CLICK]: click,
  [SVG_NAME.ARROW]: arrow,
  [SVG_NAME.ARROW_GREEN]: arrowGreen,
  [SVG_NAME.ADD_SHOPPING]: addShopping,
  [SVG_NAME.CARD_PAYMENT]: cardPayment,
  [SVG_NAME.LANGAGE]: langageIcon,
};

export default SVGs;