import SVG from './svg';
import PNG from './png';
import {fr, en} from './text';
import {LANGUAGES, LANGUAGES_COLLECTION} from "./constants";
import store from "../core/store";
import {CoreHelpers} from "../core/helpers";
import {storeKey} from "../core/constants";

// To access at n+1 string use 'key.key' template
export const findText = (key, lang) => {
  const keys = key.split('.');

  switch (lang.label) {
    case LANGUAGES.FR.label: {
      return keys.reduce((acc, k) => (acc[k] ? acc[k] : null), fr);
    }
    case LANGUAGES.EN.label: {
      return keys.reduce((acc, k) => (acc[k] ? acc[k] : null), en);
    }
    default: {
      return null;
    }
  }
}

export const injectPropsInText = (text, props) => {
  let finalText = text;
  Object.entries(props).forEach(([key, value]) => (
    finalText = text.replace(`#${key}#`, value)
  ));

  return finalText;
}

export default class AssetsHelpers {
  static getSVG(svgName) {
    return SVG[svgName];
  }
  static getPNG(pngName) {
    return PNG[pngName];
  }

  static getText(key, props, lang) {
    // Store isn't recreate at init yet
    const finalLang = lang
    || store
      ? store.getState().core.lang
      : CoreHelpers.getLocalStorage(storeKey).core.lang
    || LANGUAGES.FR;

    if (!LANGUAGES_COLLECTION.some((language) => language.label === finalLang.label)) {
      return 'Invalid Language';
    }
    const text = findText(key, finalLang);
    const textWithPropsInjected = props && text ? injectPropsInText(text, props) : text;

    return textWithPropsInjected || 'Invalid Key';
  }
}