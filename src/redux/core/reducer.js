import {LANGUAGES} from "../../assets/constants";
import actionsTypes from "./actionsType";
import {pathRoute} from "../../core/constants";

const initialState = {
  lang: LANGUAGES.FR,
  navigator: {
    location: pathRoute.HOME,
    state: {},
  }
}

export const setLang = (state, payload) => ({
  ...state,
  lang: payload.lang,
});

export const setNavigator = (state, payload) => {
  return ({
    ...state,
    navigator: {
      location: payload.location,
      state: payload.state || initialState.navigator.state,
    },
  })
}

export const reducer = (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case actionsTypes.SET_LANG:
      return setLang(state, payload);
    case actionsTypes.LOCATION_CHANGE:
      return setNavigator(state, payload);
    default:
      return state;
  }
}

export default reducer;