import {actionCreator} from "../helpers";
import actionsTypes from "./actionsType";

const actions = {
  setLang: (lang) => actionCreator(actionsTypes.SET_LANG, {lang}),
  pushLocation: (url, state) => actionCreator(actionsTypes.PUSH_LOCATION, {url, state}),
  replaceLocation: (url) => actionCreator(actionsTypes.REPLACE_LOCATION, {url}),
  backLocation: () => actionCreator(actionsTypes.BACK_LOCATION),
  locationChange: (location, state) => actionCreator(actionsTypes.LOCATION_CHANGE, {location, state}),
};

export default actions;