import coreReducer from './reducer';
import coreActions from './actions';
import coreSagas from './sagas';

const coreRedux = {
  actions: coreActions,
  reducer: coreReducer,
  sagas: coreSagas,
};

export default coreRedux;