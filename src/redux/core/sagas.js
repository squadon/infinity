import {takeLatest, call, put} from 'redux-saga/effects';

import actionsType from "./actionsType";
import actions from "./actions";
import {history} from "../../core/history";

function* locationChange(state = {}) {
  const location = history.location.pathname;

  yield put(actions.locationChange(location, state));
}

function* pushLocation(action) {
  const {url, state} = action.payload;
  yield history.push(url, state);

  yield call(locationChange, state);
}

function* replaceLocation(action) {
  const {url, state} = action.payload;
  yield history.replace(url, state);

  yield call(locationChange, state);
}

function* backLocation() {
  yield history.goBack();

  yield call(locationChange);
}

export default function* () {
  yield takeLatest(actionsType.PUSH_LOCATION, pushLocation)
  yield takeLatest(actionsType.REPLACE_LOCATION, replaceLocation)
  yield takeLatest(actionsType.BACK_LOCATION, backLocation)
}