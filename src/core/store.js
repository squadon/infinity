import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';

import {coreRedux} from '../redux';
import {storeKey} from "./constants";
import {CoreHelpers} from "./helpers";

const reducers = {
  core: coreRedux.reducer,
};

const sagas = [
  coreRedux.sagas,
]

const reduxDevTool = process.env.NODE_ENV === 'development'
  ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : null
  : null;

const sagaMiddleware = createSagaMiddleware();
const middlewares = [thunk, sagaMiddleware];

const enhanceReducer = combineReducers({...reducers});

const store = createStore(
  enhanceReducer,
  CoreHelpers.getLocalStorage(storeKey),
  compose(applyMiddleware(...middlewares), reduxDevTool),
);

sagas.forEach((saga) => sagaMiddleware.run(saga));

store.subscribe(() => CoreHelpers.saveInLocalStorage(storeKey, store.getState()));

export default store;