export class CoreHelpers {
  static saveInLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  static getLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key)) || {};
  }
}