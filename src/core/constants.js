import AssetsHelpers from "../assets/assetsHelpers";

export const storeKey = 'socle-reactJS';

export const pathRoute = {
  DEFAULT: '/'
};

export const navBar = (lang) => [
  {
    disabled: false,
    label: AssetsHelpers.getText('navbar.theme', lang),
    onClick: () => console.log('lorem'),
    //subMenu: [{label: 'ipsum', onClick: () => {}}, {label: 'ipsum2', onClick: () => {}}],
  },
  {
    disabled: true,
    label: 'site vitrine',
    onClick: () => console.log('lorem'),
    subMenu: [{label: 'ipsum', onClick: () => {}}, {label: 'ipsum2', onClick: () => {}}],
  },
  {
    disabled: true,
    label: 'lorem',
    onClick: () => console.log('lorem2'),
    subMenu: [],
  }
]