import {pathRoute} from './constants';
import {Home} from '../ui/screens';

export const routes = ({
  home: {
    path: pathRoute.DEFAULT,
    component: Home,
    auth: false,
  },
});

export default routes;