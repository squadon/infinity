import React from 'react';
import {connect} from 'react-redux';
import {Router, Route, Switch, Redirect} from 'react-router';

import {pathRoute} from './constants';
import {history} from './history';
import routes from './routes';

const createRoute = () => Object
  .entries(routes)
  .map(([key, route]) => (
    <Route exact key={key} path={route.path} component={route.component}/>
  ));


export const ConnectedRouting = () => {
  const routes = createRoute();

  return (
    <Router history={history}>
      <Switch>
        {routes}
        <Redirect to={pathRoute.DEFAULT}/>
      </Switch>
    </Router>
  );
};

const mapStateToProps = (state) => ({
  lang: state.core.lang,
})

export default connect(mapStateToProps)(ConnectedRouting);