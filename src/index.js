import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import * as serviceWorker from './serviceWorker';
import store from './core/store';
import ConnectedRouting from './core/ConnectedRouting';
import Header from "./ui/components/header";

import './index.scss';

ReactDOM.render(
  <Provider store={store}>
    <Header/>
    <ConnectedRouting/>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
