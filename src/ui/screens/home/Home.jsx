import React from 'react';

import {Showcase} from "./components/Showcase";
import {Tuto} from "./components/Tuto";
import {Possibilities} from "./components/Possibilities";
import {Footer} from "../../components/footer/Footer";

import './home.scss';

export const Home = (props) => {
  return (
    <section id="home" className="home">
      <Showcase
        onFirstStep={null}
        product={null}
        onSeeProduct={null}
      />
      <Tuto />
      <Possibilities />
      <Footer />
    </section>
  );
}