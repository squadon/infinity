import React from "react";
import {Carousel} from "../../../sharedComponents/carousel/Carousel";
import {Button} from "../../../sharedComponents/buttons/Button";
import AssetsHelpers from "../../../../assets/assetsHelpers";

export const Possibilities = (props) => {
  const carouselData = [{
    img: 'https://www.webself.net/img/accueil-anim/appareil-architecte.png',
    name: 'Site1',
    link: '',
  }, {
    img: 'https://monsiteweb.io/wp-content/uploads/2018/09/smartmockups_jlya82o7-min.png',
    name: 'Site2',
    link: '',
  }, {
    img: 'https://www.webself.net/img/accueil-anim/appareil-architecte.png',
    name: 'Site3',
    link: '',
  }, {
    img: 'https://www.webself.net/img/accueil-anim/appareil-architecte.png',
    name: 'Site4',
    link: '',
  }, {
    img: 'https://www.webself.net/img/accueil-anim/appareil-architecte.png',
    name: 'Site5',
    link: '',
  }]

  return (
    <article className="possibilities">
      <h1 className="title">Possibilities</h1>
      <Carousel data={carouselData}/>
      <Button label={AssetsHelpers.getText('home.tryIt')}/>
    </article>
  )
}