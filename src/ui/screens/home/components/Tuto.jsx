import React from "react";
import {BubbleInfo} from "../../../sharedComponents/bubbleInfo/BubbleInfo";
import {SVG_NAME} from "../../../../assets/constants";
import {Button} from "../../../sharedComponents/buttons/Button";
import AssetsHelpers from "../../../../assets/assetsHelpers";
import {BUTTONS_TYPES} from "../../../sharedComponents/buttons/constants";

// TODO a revoir, selectionner un theme, Ajouter les différents blocs, Expliquez nous vos envies, Recuperer votre site web

export const Tuto = (props) => {
  return (
    <article className="tuto">
      <div className="tuto-content">
        <BubbleInfo
          icon={SVG_NAME.ADD_SHOPPING}
          text={AssetsHelpers.getText('tuto.first')}
        />
        <BubbleInfo
          icon={SVG_NAME.CARD_PAYMENT}
          text={AssetsHelpers.getText('tuto.second')}
        />
        <BubbleInfo
          icon={SVG_NAME.LANGAGE}
          text={AssetsHelpers.getText('tuto.third')}
        />
        <BubbleInfo
          icon={SVG_NAME.LANGAGE}
          text={AssetsHelpers.getText('tuto.fourth')}
        />
      </div>
      <div className="buttons-container">
        <Button type={BUTTONS_TYPES.EDGE_PRIMARY} label={AssetsHelpers.getText('tuto.howItWorks')}/>
        <Button label={AssetsHelpers.getText('tuto.pickNow')}/>
      </div>
    </article>
  )
}