import React from "react";

import AssetsHelpers from "../../../../assets/assetsHelpers";
import {PNG_NAME, SVG_NAME} from "../../../../assets/constants";
import {Button} from "../../../sharedComponents/buttons/Button";
import {BUTTONS_TYPES} from "../../../sharedComponents/buttons/constants";
import {Video} from "../../../sharedComponents/videos/Video";

export const Showcase = (props) => {
  const {onFirstStep, product, onSeeProduct} = props;

  return (
    <article id="showcase">
      <img
        className="background"
        src={AssetsHelpers.getPNG(PNG_NAME.SHOWCASE_BACKGROUND)}
        alt="background"
      />
      <div className="content">
        <div className='first-step'>
          <p className="text description">
            {AssetsHelpers.getText('home.introduction.one')}
            <br/>
            {AssetsHelpers.getText('home.introduction.two')}
          </p>
          <Button
            type={BUTTONS_TYPES.PRIMARY}
            label={AssetsHelpers.getText('home.firstStep')}
            onClick={onFirstStep}
          />
        </div>
        <Video
          source={AssetsHelpers.getPNG(PNG_NAME.YOUTUBE_CAPTURE)}
          onSelect={onSeeProduct}
        />
      </div>
      <img className="see-bottom" src={AssetsHelpers.getSVG(SVG_NAME.ARROW_GREEN)} alt="indicator-arrow-down"/>
    </article>
  )
}
