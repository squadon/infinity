import React from "react";

import {NavBar} from "../../sharedComponents/navBar/NavBar";
import AssetsHelpers from "../../../assets/assetsHelpers";
import {PNG_NAME} from "../../../assets/constants";

import LangSelector from "../langSelector";
import {navBar} from "../../../core/constants";

import './header.scss';

export const Header = (props) => (
  <header id="header">
    <img className="background" src={AssetsHelpers.getPNG(PNG_NAME.HEADER_LINE)} alt="background"/>
    <img id='logo' src={AssetsHelpers.getPNG(PNG_NAME.LOGO)} alt="logo"/>
    <NavBar menu={navBar(props.lang)}/>
    <LangSelector/>
  </header>
)
