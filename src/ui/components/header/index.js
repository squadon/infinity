import {connect} from 'react-redux';

import {Header} from "./Header";

const mapStateToProps = (state) => ({
  lang: state.core.lang
})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
