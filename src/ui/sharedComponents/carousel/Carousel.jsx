import React, {useEffect, useRef, useState} from "react";

import AssetsHelpers from "../../../assets/assetsHelpers";
import {SVG_NAME} from "../../../assets/constants";
import {ARROW_TYPE} from "./constants";

import './carousel.scss';

export const Carousel = (props) => {
  const {data} = props;
  const list = useRef(null);
  const [current, setCurrent] = useState(Math.floor(Math.random() * Math.floor(data.length - 1)));
  const [isLast, setIsLast] = useState(false);
  const [isFirst, setIsFirst] = useState(false);
  let scrollEnd = null;

  useEffect(() => {
    list.current.scrollTo({left: current * 370, behavior: 'smooth'})
    setIsLast(current === data.length - 1);
    setIsFirst(current === 0);
  }, [current, list, setIsLast, setIsFirst, data]);

  const handleScroll = (event) => {
    const {scrollLeft} = event.currentTarget;

    clearTimeout(scrollEnd);

    scrollEnd = setTimeout(() => {
      const positionOnEnd = Math.round(scrollLeft / 370);

      if(current !== positionOnEnd) {
        setCurrent(positionOnEnd);
      } else {
        list.current.scrollTo({left: positionOnEnd * 370, behavior: 'smooth'})
      }
    }, 50);
  }

  const handleNextPrevious = (type) => {
    if (type === ARROW_TYPE.NEXT) {
      return isLast ? null : setCurrent((current) => current + 1)
    } else if (type === ARROW_TYPE.PREVIOUS) {
      return isFirst ? null : setCurrent((current) => current - 1)
    }
  }

  return (
    <div className="carousel-wrapper">
      <div className="carousel-horizontal-fade">
        <span className="arrow-wrapper" onClick={() => handleNextPrevious(ARROW_TYPE.PREVIOUS)}>
          <img className="arrow left" src={AssetsHelpers.getSVG(SVG_NAME.ARROW_GREEN)} alt={AssetsHelpers.getText('imgAlt.arrowLeft')}/>
        </span>
        <span className="arrow-wrapper" onClick={() => handleNextPrevious(ARROW_TYPE.NEXT)}>
          <img className="arrow right" src={AssetsHelpers.getSVG(SVG_NAME.ARROW_GREEN)} alt={AssetsHelpers.getText('imgAlt.arrowRight')}/>
        </span>
      </div>
      <ul ref={list} className="carousel-list" onScroll={handleScroll}>
        <li className="item"/>
        {data && data.map((item) => (
          <li key={item.name} className="item">
            <img className="item-preview" src={item.img} alt={AssetsHelpers.getText('imgAlt.preview')}/>
          </li>
        ))}
        <li className="item"/>
      </ul>
    </div>
  )
}