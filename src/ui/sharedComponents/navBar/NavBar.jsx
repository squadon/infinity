import React from "react";

import AssetsHelpers from "../../../assets/assetsHelpers";
import {SVG_NAME} from "../../../assets/constants";

import './navbar.scss';

export const NavBar = (props) => {
  const {menu} = props;

  if(!menu) {
    return null;
  }

  return (
    <ul id="navbar">
      {
        menu.map((item) => {
          const {label, subMenu, onClick, disabled} = item;
          if(disabled) {
            return null;
          }
          return (
            <li key={label} className="item" onClick={onClick}>
              <div className="head-item">
                <p className="head-text">{label}</p>
                {subMenu && subMenu.length > 0 && <img className="arrow-down" src={AssetsHelpers.getSVG(SVG_NAME.ARROW)} alt={AssetsHelpers.getText('imgAlt.arrow')}/>}
              </div>
              {subMenu && (
                <ul className="sub-menu">
                  {subMenu.map((sub) => (
                    <li key={sub.label} className="sub-item" onClick={sub.onClick}>
                      {sub.label}
                    </li>
                  ))}
                </ul>
              )}
            </li>
          )
        })
      }
    </ul>
  )
}