import React from "react";
import classNames from "classnames";

import {BUTTONS_TYPES} from "./constants";

import './buttons.scss';

export const Button = (props) => {
  const {label, type, disabled, onClick} = props;

  const finalType = Object.values(BUTTONS_TYPES).find((t) => t === type)
    ? type
    : BUTTONS_TYPES.PRIMARY;

  const buttonClass = classNames({
    'button': true,
    [finalType]: true,
    'disabled': disabled,
  })

  const handleClick = (e) => {
    e.stopPropagation();

    if (!disabled && onClick) {
      onClick();
    }
  }

  return (
    <div className="button-wrapper">
      <span
        className={buttonClass}
        onClick={handleClick}
      >
      <p className="label">{label}</p>
    </span>
    </div>
  )
}

Button.defaultProps = {
  disabled: false,
  type: BUTTONS_TYPES.PRIMARY,
}