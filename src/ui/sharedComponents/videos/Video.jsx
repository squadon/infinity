import React from "react";

import {Button} from "../buttons/Button";
import AssetsHelpers from "../../../assets/assetsHelpers";

import './videos.scss';

export const Video = (props) => {
  const {source, onSelect} = props;

  return (
    <div className="video-wrapper">
      <span className="video-modal">
        <Button label={AssetsHelpers.getText('home.see')} onClick={onSelect}/>
      </span>
      <img
        className="video"
        src={source}
        alt={AssetsHelpers.getText('imgAlt.video')}
      />
    </div>
  )
}