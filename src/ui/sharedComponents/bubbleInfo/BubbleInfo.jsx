import React from "react";
import AssetsHelpers from "../../../assets/assetsHelpers";

import './bubbleInfo.scss';

export const BubbleInfo = (props) => {
  const {icon, text} = props;
  return (
    <div className="bubble-info-wrapper">
      <span className="bubble">
        <img className="bubble-icon" src={AssetsHelpers.getSVG(icon)} alt={AssetsHelpers.getText('imgAlt.icon')}/>
      </span>
      <p className="bubble-info-text">
        {text}
      </p>
    </div>
  )
}